import React from 'react'
import { BrowserRouter,Switch} from 'react-router-dom'
import { Route } from 'react-router'
import DashboardLayout from './route/DashboardLayout'
import Auth from "./pages/Auth/Auth";
import Login from "./pages/Auth/Login";

export default function App() {
  return (
    <div>
      <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Login}/>
            <Route exact path="/auth" component={Auth}/>
            <Route exact path="/admin" component={DashboardLayout}/>
          </Switch>
      </BrowserRouter>
    </div>
  )
}
