import React, { useEffect, useState } from 'react'
import MUIDataTable from 'mui-datatables';
import { Box, Button, Typography } from '@material-ui/core';
import firebase from '../../firebase';
import { DBREFADMIN } from '../../constants/constant';

export default function ListCenseurs() {
    const colums = ['moms','email','matricule']
    const data = [
        ["Joe James", "Test Corp", "Yonkers", "NY"],
        ["John Walsh", "Test Corp", "Hartford", "CT"],
        ["Bob Herm", "Test Corp", "Tampa", "FL"],
        ["James Houston", "Test Corp", "Dallas", "TX"],
       ];
    const [list, setList] = useState([])
    useEffect(() => {
       DBREFADMIN
            .where('grade', '==', 'Censeur')
            .onSnapshot(snapshot => {
                const listTab=[];
                snapshot.docs.forEach(doc => {
                    const tab=[];
                    tab.push(doc.data().name);
                    tab.push(doc.data().email);
                    tab.push(doc.id);
                    listTab.push(tab)
                })
                setList(listTab)
                
            })
    }, [])
    return (
        <div>
            <Box m={2}>
                <Typography align="right">
                <Button color="primary" variant="contained">
                    Ajouter censeurs
                </Button>
            </Typography>
            </Box>
            <MUIDataTable title={"liste des censeurs"}
                columns={colums}
                data={list}
            />
            {console.log('bonjouruuuuuuuuuuuuuuuuuuuuu')}
        </div>
    )
}
