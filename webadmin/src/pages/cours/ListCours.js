import React, { useEffect } from 'react'
import MUIDataTable from 'mui-datatables';
import { Box, Button, Typography } from '@material-ui/core';
import firebase from '../../firebase'

export default function ListCours() {
    const colums = ['moms','professeurs','code']
    const data = [
        ["histoire", "john doe", "hist22"],
       ];

    useEffect(() => {
        firebase
            .firestore()
            .collection('user-admin')
            
    }, [])
    return (
        <div>
            <Box m={2}>
                <Typography align="right">
                    <Button color="primary" variant="contained">
                        Ajouter cours
                    </Button>
                </Typography>
            </Box>
            <MUIDataTable title={"liste des cours"}
                columns={colums}
                data={data}
            />
        </div>
    )
}
