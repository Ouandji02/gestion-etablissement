import { Box, Button, Typography } from '@material-ui/core'
import MUIDataTable from 'mui-datatables'
import React from 'react'

export default function ListEleves(props) {
    const colums = ['moms','prenoms','matricule']
    const data = [
        ["Joe James", "Test Corp", "Yonkers", "NY"],
        ["John Walsh", "Test Corp", "Hartford", "CT"],
        ["Bob Herm", "Test Corp", "Tampa", "FL"],
        ["James Houston", "Test Corp", "Dallas", "TX"],
       ];
    return (
        <div>
            <Box m={2}>
                <Typography align="right">
                    <Button color="primary" variant="contained">
                        Ajouter un élève 
                    </Button>
                </Typography>           
            </Box>
            <Box mt={2}>
              <Typography align="left">
                    <Button color="primary" variant="contained" onClick={ ()=> { props.history.push('/admin/listCours')}}>
                        Voir matières 
                    </Button>
                </Typography>  
            </Box>
            <MUIDataTable title={"liste des élèves"}
                columns={colums}
                data={data}
            />
        </div>
    )
}
