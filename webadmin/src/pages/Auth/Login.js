import React from 'react';
import {Button, Card, CircularProgress, Grid, TextField, Typography, withStyles} from "@material-ui/core";
import firebase from '../../firebase';
import {Alert} from "@material-ui/lab";


const useStyles = {
     form: {
        marginLeft: '15%',
        marginRight: 'auto',
        marginBottom: "20px",
        marginTop: '5%',
        width: '70%'
    },
    typo: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontFamily: 'Quicksand',
        marginBottom: '20px',
        marginTop: '20px'
    },
    card: {
        paddingLeft: '20px',
        paddingRight: '20px'
    },
     btn: {
        marginLeft: '15%',
        marginRight: '20%',
        marginBottom: '20px',
        marginTop: '20px',
        width: '70%'
    },
    field: {

    }
}

class Login extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
        this.state = {
            user:{
                email: '',
                password: ''
            },
            isCircularProgress: false,
            isError: false,
            isSuccess: false
        }
    }

    handleChange = (e) => {
        console.log([e.target.name],e.target.value);
        this.setState((prevState) => ({
            user: {...prevState.user, [e.target.name]: e.target.value}
        }))
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({isCircularProgress: true});
        this.setState({isError: false});
        const { user } = this.state;
         if(user.email !== '' && user.password !== ''){
             firebase
                .auth()
                .signInWithEmailAndPassword(user.email,user.password)
                .then((response) => {
                    this.props.history.push('/admin');
                }).catch((e) => {
                    console.log(e);
                });
         }else{
             this.setState({isError: true});
             this.setState({isCircularProgress: false});
         }

    }

    render() {
        const { classes } = this.props;
        const { user } = this.state;
        return (
            <form className={ classes.form }>
                <Card className={ classes.card }>
                    <Typography variant="h5" className={ classes.typo }> Connexion sur High School </Typography>
                     <Grid container >
                        <Grid item lg={12} md={12} sm={12} xs={12}>
                            <TextField
                                name="email"
                                label="email"
                                type="email"
                                fullWidth
                                variant="outlined"
                                value={ user.email }
                                required
                                onChange={this.handleChange}
                            />
                        </Grid>
                        <Grid item lg={12} md={12} sm={12} xs={12}>
                            <TextField
                                name="password"
                                label="password"
                                type="password"
                                fullWidth
                                variant="outlined"
                                value={ user.password }
                                required
                                style={{marginTop: '20px'}}
                                onChange={this.handleChange}
                            />
                        </Grid>
                         <Grid lg={12} md={12} sm={12} xs={12}>
                            <Button
                                className={ classes.btn }
                                variant="contained" color="primary"
                                onClick={this.handleSubmit}
                                disabled={this.state.isCircularProgress && <CircularProgress/>}>
                                {this.state.isCircularProgress && <CircularProgress/>} CONNEXION
                            </Button>
                         </Grid>
                    </Grid>
                     {this.state.isSuccess &&
                     <Grid item lg={6} md={12} sm={12} xs={12} className={ classes.alert }>
                         <Alert severity="success">
                             {user.name + " " + user.surname + " "} Votre inscription a été réussie !
                         </Alert>
                     </Grid>}
                    {this.state.isError && <Grid item lg={6} md={12} sm={12} xs={12} className={ classes.alert }>
                        <Alert severity="error">
                            {user.name + " " + user.surname + " "} Certains champs ne sont pas correctement remplis
                        </Alert>
                    </Grid>}
                </Card>
            </form>
        );
    }
}

export default withStyles(useStyles, {withTheme: true}) (Login);