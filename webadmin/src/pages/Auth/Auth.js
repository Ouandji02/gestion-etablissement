import React from "react";
import {
  TextField,
  Grid,
  MenuItem,
  Card,
  withStyles,
  Button,
  CircularProgress,
  Typography,
} from "@material-ui/core";
import firebase from "../../firebase";
import { Alert } from "@material-ui/lab";
import { Link } from "react-router-dom";

const useStyles = {
  card: {
    paddingLeft: "20px",
    paddingRight: "20px",
  },
  form: {
    marginLeft: "15%",
    marginRight: "auto",
    marginBottom: "20px",
    marginTop: "5%",
    width: "70%",
  },
  btn: {
    marginLeft: "15%",
    marginRight: "20%",
    marginBottom: "20px",
    marginTop: 0,
    width: "70%",
  },
  alert: {
    marginBottom: "20px",
    marginLeft: "10%",
  },
  typo: {
    textAlign: "center",
    fontWeight: "bold",
    fontFamily: "Quicksand",
  },
};

class Auth extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newUser: {
        name: "",
        surname: "",
        sex: "",
        dateBorn: "",
        matricule: "",
        speciality: "",
        salary: "",
        dateRecrutement: "",
        grade: "",
        status: "",
        password: "",
        email: "",
      },
      circularVisible: false,
      isSuccess: false,
      isError: false,
    };
  }

  handleChange = (e) => {
    this.setState((prevState) => ({
      newUser: { ...prevState.newUser, [e.target.name]: e.target.value },
    }));
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ circularVisible: true });
    this.setState({ isError: false });
    const { newUser } = this.state;
    if (
      newUser.name !== "" &&
      newUser.surname !== "" &&
      newUser.sex !== "" &&
      newUser.email !== "" &&
      newUser.password !== "" &&
      newUser.dateBorn !== "" &&
      newUser.matricule !== "" &&
      newUser.spacing !== "" &&
      newUser.salary !== "" &&
      newUser.dateRecrutement !== "" &&
      newUser.grade !== ""
    ) {
      const { password, ...users } = newUser;
      // eslint-disable-next-line no-unused-expressions
      firebase
        .auth()
        .createUserWithEmailAndPassword(newUser.email, newUser.password)
        .then((user) => {
          console.log(user.uid);
          firebase
            .firestore()
            .collection("users-admin")
            .doc(user.uid)
            .set(users, { merge: true });
          this.setState({ isSuccess: true });
          this.setState({ circularVisible: false });
        })
        .catch((e) => {
          console.log(e.message);
        });
    } else {
      this.setState({ circularVisible: false });
      this.setState({ isError: true });
    }
  };

  render() {
    const speciality = ["Anglais", "Français", "Physique", "Chimie"];
    const grade = [
      "Proviseur",
      "Censeur",
      "Surveillant General",
      "Surveillant de secteur",
      "Enseignant",
    ];
    const status = ["Vacataire", "Permanent"];
    const sex = ["Masculin", "Feminin"];

    const { classes } = this.props;
    const { newUser } = this.state;
    console.log(newUser);
    return (
      <>
        <form className={classes.form}>
          <Card shadow={5} className={classes.card}>
            <Typography
              variant="h5"
              className={classes.typo}
              style={{ marginTop: "20px" }}
            >
              Connexion sur High School{" "}
            </Typography>
            <Grid container spacing={3}>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  name="name"
                  onChange={this.handleChange}
                  variant="outlined"
                  label="Nom"
                  required
                  value={newUser.name}
                  fullWidth
                  style={{ marginTop: "20px" }}
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  name="surname"
                  variant="outlined"
                  onChange={this.handleChange}
                  label="Prenom"
                  required
                  value={newUser.surname}
                  fullWidth
                  style={{ marginTop: "20px" }}
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  name="email"
                  tupe="email"
                  variant="outlined"
                  onChange={this.handleChange}
                  label="Email"
                  required
                  value={newUser.email}
                  fullWidth
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  name="password"
                  type="password"
                  variant="outlined"
                  onChange={this.handleChange}
                  label="Mot de passe"
                  required
                  value={newUser.password}
                  fullWidth
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  name="sex"
                  select
                  label="Sexe"
                  onChange={this.handleChange}
                  fullWidth
                  required
                  value={newUser.sex}
                  variant="outlined"
                  // value={currency}
                  // onChange={handleChange}
                  helperText="Selectionner Votre sexe"
                >
                  {sex.map((grade) => (
                    <MenuItem key={grade} value={grade}>
                      {grade}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  label="Date de naissance"
                  name="dateBorn"
                  type="date"
                  variant="outlined"
                  fullWidth
                  required
                  value={newUser.dateBorn}
                  onChange={this.handleChange}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  name="matricule"
                  variant="outlined"
                  label="Matricule"
                  fullWidth
                  required
                  value={newUser.matricule}
                  onChange={this.handleChange}
                  variant="outlined"
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  name="speciality"
                  select
                  label="Spécialité"
                  fullWidth
                  required
                  value={newUser.speciality}
                  onChange={this.handleChange}
                  variant="outlined"
                  // value={currency}
                  // onChange={handleChange}
                  helperText="Selectionner Votre sexe"
                >
                  {speciality.map((grade) => (
                    <MenuItem key={grade} value={grade}>
                      {grade}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  name="salary"
                  variant="outlined"
                  type="number"
                  required
                  value={newUser.salary}
                  label="Salaire"
                  fullWidth
                  onChange={this.handleChange}
                  variant="outlined"
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  label="Date de recrutement"
                  name="dateRecrutement"
                  type="date"
                  required
                  value={newUser.dateRecrutement}
                  variant="outlined"
                  onChange={this.handleChange}
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextField
                  className={classes.field}
                  name="grade"
                  select
                  label="Grade"
                  fullWidth
                  variant="outlined"
                  required
                  value={newUser.grade}
                  onChange={this.handleChange}
                  // value={currency}
                  // onChange={handleChange}
                  helperText="Selectionner Votre sexe"
                >
                  {grade.map((grade) => (
                    <MenuItem key={grade} value={grade}>
                      {grade}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              {newUser.grade === "Enseignant" && (
                <Grid item lg={6} md={6} sm={12} xs={12}>
                  <TextField
                    className={classes.field}
                    name="status"
                    select
                    label="Status"
                    fullWidth
                    variant="outlined"
                    required
                    value={newUser.status}
                    onChange={this.handleChange}
                    // value={currency}
                    // onChange={handleChange}
                    helperText="Selectionner Votre satus"
                  >
                    {status.map((grade) => (
                      <MenuItem key={grade} value={grade}>
                        {grade}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>
              )}
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.btn}
                  disabled={this.state.circularVisible}
                  onClick={this.handleSubmit}
                >
                  {this.state.circularVisible ? (
                    <CircularProgress color="primary" />
                  ) : (
                    "S'inscrire"
                  )}
                </Button>
              </Grid>
              <Typography
                className={classes.typo}
                variant="h6"
                style={{ marginBottom: "20px" }}
              >
                Vous avez déja un compte?
                <Link to="/">Connectez Vous</Link>
              </Typography>
              {this.state.isSuccess && (
                <Grid
                  item
                  lg={6}
                  md={12}
                  sm={12}
                  xs={12}
                  className={classes.alert}
                >
                  <Alert severity="success">
                    {newUser.name + " " + newUser.surname + " "} Votre
                    inscription a été réussie !
                  </Alert>
                </Grid>
              )}
              {this.state.isError && (
                <Grid
                  item
                  lg={6}
                  md={12}
                  sm={12}
                  xs={12}
                  className={classes.alert}
                >
                  <Alert severity="error">
                    {newUser.name + " " + newUser.surname + " "} Certains champs
                    ne sont pas correctement remplis
                  </Alert>
                </Grid>
              )}
            </Grid>
          </Card>
        </form>
      </>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(Auth);
