import React, { useEffect, useState } from 'react'
import MUIDataTable from 'mui-datatables';
import { Box, Button, Typography } from '@material-ui/core';
import { DBREFADMIN } from '../../constants/constant';

export default function ListProfesseurs() {
    const colums = ['moms','email','matricule']
    const data = [
        ["Joe James", "Test Corp", "Yonkers", "NY"],
        ["John Walsh", "Test Corp", "Hartford", "CT"],
        ["Bob Herm", "Test Corp", "Tampa", "FL"],
        ["James Houston", "Test Corp", "Dallas", "TX"],
       ];

       const [list, setList] = useState([])
       useEffect(() => {
          DBREFADMIN
               .where('grade', '==', 'Enseignant')
               .onSnapshot(snapshot => {
                   const listTab=[];
                   snapshot.docs.forEach(doc => {
                       const tab=[];
                       tab.push(doc.data().name);
                       tab.push(doc.data().email);
                       tab.push(doc.id);
                       listTab.push(tab)
                   })
                   setList(listTab)
                   
               })
       }, [])

    return (
        <div>
            <Box m={2}>
                <Typography align="right">
                <Button color="primary" variant="contained">
                    Ajouter professeurs
                </Button>
            </Typography>
            </Box>
            <MUIDataTable title={"liste des professeurs"}
                columns={colums}
                data={list}
            />
        </div>
    )
}
