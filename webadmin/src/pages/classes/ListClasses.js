import { Box, Grid, Typography } from '@material-ui/core'
import React from 'react'
import DashboardItem from '../../components/DashbordItem'

export default function ListClasses() {
    return (
        <div>
            <Box m={2}>
                <Typography variant="h5" align="left">Listes des classes</Typography>
            </Box>
            <Grid container spacing={2} direction="row">
                    <DashboardItem title="6eme" to="/admin/listEleves"/>
                    <DashboardItem title="5eme" />
                    <DashboardItem title="4eme" />
                    <DashboardItem title="3emeA1" />
                    <DashboardItem title="3emeA2" />
            </Grid>
            
        </div>
    )
}
