import React from 'react';
import { Button } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import firebase from '../firebase'

export default function Logout() {

     const logout = () => {
        firebase.auth().signOut()
    }

    return (
        <Button onClick={logout} color="secondary" variant="contained"><ExitToAppIcon color="inherit" /></Button>
    )
}

