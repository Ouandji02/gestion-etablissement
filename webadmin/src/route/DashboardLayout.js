import React from 'react'
import { Route, Switch } from 'react-router'
import Accueil from '../pages/Accueil'
import ListCenseurs from '../pages/censeurs/ListCenseurs'
import ListClasses from '../pages/classes/ListClasses'
import ListCours from '../pages/cours/ListCours'
import Dashboard from '../pages/Dashboard'
import ListEleves from '../pages/eleves/ListEleves'
import ListProfesseurs from '../pages/professeurs/ListProfesseurs'
import ListSurveillants from '../pages/Surveillants/ListSurveillants'
import MenuCompte from './MenuCompte'
/* import React from 'react';
import { Route, Switch } from 'react-router';
import Accueil from '../pages/Accueil';
import Dashboard from '../pages/Dashboard';
import MenuCompte from './MenuCompte'; */

export default function DashboardLayout() {
    return (
        <div>
            <Switch>
                <MenuCompte>
                    <Route exact path="/admin" component={Dashboard}/>
                    <Route exact path="/admin/Professeurs" component={ListProfesseurs}/>
                    <Route exact path="/admin/censeurs" component={ListCenseurs}/>
                    <Route exact path="/admin/surveillants" component={ListSurveillants}/>
                    <Route path="/admin/classes" component={ListClasses}/>
                    <Route path="/admin/listEleves" component={ListEleves}/>
                    <Route path="/admin/listCours" component={ListCours}/>
                </MenuCompte>
            </Switch>
        </div>
    )
}
