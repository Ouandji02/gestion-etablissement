import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Box, Button, Grid, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  avatar: {
    background: theme.palette.primary.main,
    cursor: 'pointer',
    marginLeft: theme.spacing(2)
  }
}));

const DashboardItemClasses = ({ title, Icon, count, to, props }) => {
  const classes = useStyles();

  return (
    <Grid item xs={12} sm={6} md={4}>
      <Button variant="contained" onClick={() => { alert('bonjour')}}>
      <Card elevation={2}>
        <CardHeader
          avatar={
            <Avatar className={classes.avatar} component={Link} to={to}>
              {Icon}
            </Avatar>
          }
          title={title}
          subheader="Effectif"
        />
        <CardContent>
          <Typography
            variant="body2"
            color="textSecondary"
            component={Box}
            fontWeight="fontWeightBold"
          >
            {count}
          </Typography>
        </CardContent>
      </Card>
      </Button>  
      
    </Grid>
  );
};

export default DashboardItemClasses;
