module.exports = {

    addObject : function(db, typeObject, data){
        return db
                .collection(typeObject)
                .doc()
                .set(data)
    },

    updateObjetc : function(db, typeObject, id, data){
        return db
                .collection(typeObject)
                .doc(id)
                .update(data)
                .then(function(res){
                    console.log('reussi')
                })
                .catch(function(err){
                    console.log(err)
                })
    }, 

    deleteObject : function(db, typeOject, id){
        return db
                .collection(typeObject)
                .doc(id)
                .delete()
    },

    readObject : async function(db,typeObject){
         await db
                .collection(typeObject)
                .get()
    },

    readObjetWithId : async function(db,typeObjet, id){
        return await db
               .collection(typeObjet)
               .where('id','==',id)
               .get()
               .data()
    },

    readObjetWithGrade : async function(db,typeObjet, grade){
        return await db
               .collection(typeObjet)
               .where('grade','==',grade)
               .get()
    }
}