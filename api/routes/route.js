const express = require("express");
const cors = require('cors')
const body_parser = require('body-parser')
const serviceAccount = require('../gestion-etablissement-scolaire-firebase-adminsdk-mba5m-b3714f8262.json')
const admin = require('firebase-admin')
const ADMINISTRATEUR = 'users-admin'
const ELEVE = 'Eleve'
const COURS = 'cours'
const CLASSES = 'Classes'
const data = require('../fonctions/fonctions')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})

const db = admin.firestore()
const route = express()

route.use(cors())
route.use(body_parser.json())

route.get('/', function (req, res) {
    res.send('hello everyone')
})

route.post('/add-administrateur', (req, res) => {
    data.addObject(db,ADMINISTRATEUR,datas)
})

route.post('/add-classe', (req, res) => {
    data.addObject(db,CLASSES,datas)
})

route.post('/add-eleve', (req, res) => {
    data.addObject(db,ELEVE,datas)
})

route.post('/add-cours', (req, res) => {
    data.addObject(db,COURS,datas)
})


route.post('/update-adminsitrateur/:id', (req, res) => {
    res.send('update')
    const id = req.params.id
    data.updateObjetc(db, ADMINISTRATEUR, id, datas)
})

route.post('/update-classe/:id', (req, res) => {
    res.send('update')
    const id = req.params.id
    data.updateObjetc(db, CLASSES, id, datas)
})

route.post('/update-cours/:id', (req, res) => {
    res.send('update')
    const id = req.params.id
    data.updateObjetc(db, COURS, id , datas)
})

route.post('/update-eleve/:id', (req, res) => {
    res.send('update')
    const id = req.params.id
    data.updateObjetc(db, ELEVE, id, datas)
})

route.get('/readAll-classe',function(req,res){
    res.send('read')
    data.readObject(db, CLASSES)
})

route.get('/readAll-cours',function(req,res){
    res.send('read')
    data.readObject(db, COURS)

})

route.get('/readAll-eleve',function(req,res){
    res.send('read')
    data.readObject(db, ELEVE)

})

route.get('/read-administrateur',function(req,res){
    res.send('read')
    const id=''
    data.readObjetWithId(db, ADMINISTRATEUR, id)

})

route.get('/readAll-administrateur',function(req,res){
    res.send('read')
    const grade=''
    data.readObjetWithGrade(db, ADMINISTRATEUR, grade)

})

route.get('/read-classe',function(req,res){
    res.send('read')
    const id=''
    data.readObjetWithId(db, CLASSES, id)

})

route.get('/read-cours',function(req,res){
    res.send('read')
    const id=''
    data.readObjetWithId(db, COURS, id)

})

route.get('/read-eleve',function(req,res){
    res.send('read')
    const id=''
    data.readObjetWithId(db, ELEVE, id)

})

route.delete('/delete-administrateur/:id', function(req,res){
    const id = req.params.id
    data.deleteObject(db,ADMINISTRATEUR,id)
})

route.delete('/delete-classe/:id', function(req,res){
    const id = req.params.id
    data.deleteObject(db,CLASSES,id)
})

route.delete('/delete-cours/:id', function(req,res){
    const id = req.params.id
    data.deleteObject(db,COURS,id)
})

route.delete('/delete-eleve/:id', function(req,res){
    const id = req.params.id
    data.deleteObject(db,ELEVE,id)
})


module.exports = route